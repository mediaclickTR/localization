<?php


namespace Mediapress\Locale;


use Illuminate\Support\ServiceProvider;

class LocalizationServiceProvider extends ServiceProvider
{


    public function boot()
    {

        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Localization' => resource_path('lang')], 'Localization');

    }

}